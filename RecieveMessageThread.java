import Xml.XMLParser;
import java.io.BufferedReader;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.Future;

public class RecieveMessageThread extends Thread {

  BufferedReader serverIn;
  XMLParser parser;

  RecieveMessageThread(BufferedReader serverIn) {
    this.serverIn = serverIn;
    this.parser = new XMLParser();
    this.start();
  }

  public void run() {
    try {
      String response;
      while (true) {
        response = serverIn.readLine();
        if (parser.isTagPresent(response, "ChatApp")) {
          String responseMessagePacket = response;
          String from = parser.getTagData(responseMessagePacket, "From");
          String messageBody = parser.getTagData(responseMessagePacket, "Body");
          System.out.println("Message From: " + from);
          System.out.println("Message Body: " + messageBody);
        } else {
          System.out.println("Response from server: " + response);
        }
      } 
    } catch (SocketException se) {
      System.out.println("Socket Exception Caught: " + se);
    } catch (Exception e) {
      System.out.println("Exception Caught: " + e);
    }
    // try {
    //   serverIn.close();
    // } catch (IOException e) {
    //   e.printStackTrace();
    // }
  }
}
