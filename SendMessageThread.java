import Xml.XMLBuilder;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.Future;

public class SendMessageThread extends Thread {

  BufferedReader br;
  PrintWriter serverOut;
  String username;
  String messagePacket;
  XMLBuilder builder;

  SendMessageThread(BufferedReader br, PrintWriter serverOut, String username) {
    this.br = br;
    this.serverOut = serverOut;
    this.username = username;
    this.start();
  }

  public void run() {
    try {
      while (true) {
        System.out.println("ChatApp:");
        System.out.println("\tPress 1 to chat");
        System.out.println("\tPress 2 to exit");
        String caseNo = br.readLine();
        switch (caseNo) {
          case "1":
            System.out.println("You selected to chat");
            sendMessage();
            break;
          case "2":
            System.out.println("You selected to Exit");
            break;
          default:
            break;
        }
      }
      // br.close();
      // serverOut.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void sendMessage() throws Exception {
    System.out.print("Enter username to whom you want send message: ");
    String to = "";
    to = br.readLine();
    while (to.length() == 0) {
      System.out.println("To field cannot be empty");
      System.out.print("Enter username to whom you want send message: ");
      to = br.readLine();
    }

    System.out.print("Enter message body: ");
    String messageBody = br.readLine();
    while (messageBody.length() == 0) {
      System.out.println("Message body cannot be empty");
      System.out.print("Enter message body: ");
      messageBody = br.readLine();
    }


    String messageFormat =
      "<ChatApp><Username></Username><Message type='message'><To></To><Body></Body></Message></ChatApp>";
    XMLBuilder builder = new XMLBuilder(messageFormat);

    builder.insertDataIntoXMLFormat(username, "Username").insertDataIntoXMLFormat(to, "To").insertDataIntoXMLFormat(messageBody, "Body");

    String messagePacket = builder.getXMLFormatWithData();

    System.out.println("Sending...");
    serverOut.println(messagePacket);
  }
}
