package Xml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLBuilder {

    private StringBuilder xmlFormat;
    private StringBuilder xmlFormatWithData;

    public XMLBuilder(String formatString) {
        this.xmlFormat = new StringBuilder(formatString);
        this.xmlFormatWithData = new StringBuilder(formatString);
    }
    

    public XMLBuilder insertDataIntoXMLFormat(String contentToBeInserted, String startingTag) {
        String startingTagGroup = "(<" + startingTag + ">)";
        String regex = startingTagGroup;
        Pattern dataPattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher dataMatcher = dataPattern.matcher(xmlFormatWithData);
        if(dataMatcher.find()) {
            xmlFormatWithData.insert(dataMatcher.end(), contentToBeInserted);
        }
        return this;
    }

    public String getXMLFormatWithData() {
        return xmlFormatWithData.toString();
    }
    
    public StringBuilder getXMLFormat() {
        return xmlFormat;
    }

    public void setXMLFormat(StringBuilder format) {
        this.xmlFormat = format;
    }

}
