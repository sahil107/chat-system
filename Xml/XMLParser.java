package Xml;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * XMLParser
 */
public class XMLParser {

    public boolean isTagPresent(String content, String tagName) {
        String regex = prepareRegexForTag(tagName);
        Pattern dataPattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher dataMatcher = dataPattern.matcher(content);
        return dataMatcher.find();
    }

    public String getTagData(String content, String tagName) {
        String regex = prepareRegexForTag(tagName);
        return getData(regex, content);
    }

    public Vector<String> getAllTagsData(String content, String tagName) {
        String regex = prepareRegexForTag(tagName);
        return getAllData(regex, content);
    }
   
    private String getData(String regex, String content) {

        Pattern dataPattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher dataMatcher = dataPattern.matcher(content);
        String result = "";
        if(dataMatcher.find()) {
            result = dataMatcher.group(2); 
        }
        return result;
    }
    
    private Vector<String> getAllData(String regex, String content) {

        Vector<String> data = new Vector<>();
        Pattern dataPattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher dataMatcher = dataPattern.matcher(content);
        while(dataMatcher.find()) {
            data.add(dataMatcher.group(2));
        }

        return data;
    }

    private String prepareRegexForTag(String tagName) {
        String startingTagGroup = "(<" + tagName + ">)";
        String endingTagGroup = "(</" + tagName + ">)";
        String innerContentRegexGroup = "(.*?)";
        String regex = startingTagGroup + innerContentRegexGroup + endingTagGroup;
        return regex;
    }
}