import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientCode {

  static String username;

  public static void main(String[] args) throws UnknownHostException, IOException {
    try {
      Socket clientSocket = new Socket("127.0.0.1", 9000);

      System.out.println("Client started!!!");

      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      BufferedReader serverIn = new BufferedReader(
        new InputStreamReader(clientSocket.getInputStream())
      );
      PrintWriter serverOut = new PrintWriter(
        clientSocket.getOutputStream(),
        true
      );

      String response = "";
      while (!Boolean.valueOf(response)) {
        System.out.println("ClientSide: Enter your username: ");
        username = br.readLine();
        serverOut.println(username);
        System.out.println(
          "ClientSide: Server is validating your username.. Wait..."
        );
        response = serverIn.readLine();
        System.out.println("ClientSide: Response from server: " + response);
      }
      System.out.println("Validated");

      new SendMessageThread(br, serverOut, username);
      new RecieveMessageThread(serverIn);
      
      // clientSocket.close();
    } catch (ConnectException e) {
      System.out.println("Connection to server failed");
    }
  }
}
