import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import Xml.XMLBuilder;
import Xml.XMLParser;

public class Echoer extends Thread {

  private Socket clientSocket;
  private HashMap<String, Socket> users;
  private XMLParser parser;
  String messagePacket;
  String clientUsername;

  Echoer(Socket clientSocket, HashMap<String, Socket> users) {
    this.clientSocket = clientSocket;
    this.users = users;
    this.parser = new XMLParser();
    this.start();
  }

  public void run() {
    try {
      BufferedReader clientIn = new BufferedReader(
        new InputStreamReader(clientSocket.getInputStream())
      );
      PrintWriter clientOut = new PrintWriter(
        clientSocket.getOutputStream(),
        true
      );

      while (users.containsKey(clientUsername = clientIn.readLine())) {
        System.out.println("User: " + clientUsername + " is already online!!!");
        clientOut.println("Your are already online from another device");
      }
      System.out.println("User: " + clientUsername + " is now online!!!");
      users.put(clientUsername, clientSocket);
      clientOut.println(String.valueOf(true));

      while (
        !"exit".equalsIgnoreCase(this.messagePacket = clientIn.readLine())
      ) {
        System.out.println("Server got: " + messagePacket);
        if(parser.isTagPresent(this.messagePacket, "ChatApp")) {
          String from = parser.getTagData(this.messagePacket, "Username");
          String to = parser.getTagData(this.messagePacket, "To");
          String messageBody = parser.getTagData(this.messagePacket, "Body");

          if (users.containsKey(to)) {
            String messageFormat = "<ChatApp><Username></Username><Message><From></From><Body></Body></Message></ChatApp>";
            XMLBuilder builder = new XMLBuilder(messageFormat);

            builder.insertDataIntoXMLFormat(to, "Username").insertDataIntoXMLFormat(from, "From").insertDataIntoXMLFormat(messageBody, "Body");

            String recieverMessagePacket = builder.getXMLFormatWithData();

            Socket recieverClient = users.get(to);

            PrintWriter recieverClientOut = new PrintWriter(
              recieverClient.getOutputStream(),
              true
            );
            recieverClientOut.println(recieverMessagePacket);
          } else {
            clientOut.println("Server: " + to + " is offline");
          }
        } else {
          System.out.println("Invalid Message packet");
        }

        
      }

      System.out.println("Client closed!");
      clientIn.close();
      clientOut.close();
      clientSocket.close();
    } catch (SocketException se) {
      users.remove(clientUsername);
      System.out.println("Server: " + clientUsername + " went offline");
    } catch (IOException ie) {
      System.out.println("S.E: " + ie);
    }
  }
}
